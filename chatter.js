'use strict';

/*
TODO:
- Keep a recent digest of updates
- Build chatter strategy where current host can lose interest

TECHINCAL TODO:
- Use underscorejs instead of for loops.
- Use promises instead of callbacks
- Look into using immutable datastructures (like mori)
-*** Need to make Date UTC
*/

var request = require('request');
var _u = require('underscore');

var Chatter = function(seed, interval, interest){
  this._defaultInterest = interest;
  this._interest = this._defaultInterest;
  this._interval = interval;
  this._intervalHandlers = [];
  this.seedPeers(seed);
};

Chatter.prototype.start = function() {
  this._intervalHandlers.push([
    setInterval(this.simpleChatter.bind(this), this._interval * 5),
    setInterval(this.complexChatter.bind(this), this._interval)
  ]);
}

Chatter.prototype.stop = function() {
  if(!this._intervalHandlers || !this._intervalHandlers.length) return;

  for(var i = 0; i < this._intervalHandlers.length; i++) {
    clearInterval(this._intervalHandlers[i]);
  }
}

Chatter.prototype.getAlivePeers = function() {
  var alive = this.getOtherPeersByStatus('ALIVE');
  alive.push(this.address); // Need to add this peer to the list as well
  return alive;
}

Chatter.prototype.getDeadPeers = function() {
  return this.getOtherPeersByStatus('DEAD');
}

Chatter.prototype.prepareMessage = function() {
  return {
    peers: this.peers,
    address: this.address
  }
}

Chatter.prototype.updateState = function(state) {
  /*
    There are a few conditions under which this would update state:
    1. If there is a key with a timestamp greater than the current key
    2. If there is a key that's not present here at all.
    3. If a dead peer (only checks for the sending peer) is suddenly alive now.
  */

  //console.log("State: ", state);
  var peers = Object.keys(state['peers']);
  if(this.isNewPeer(state['peers'][state['address']])) {
    console.log("Sender has revived! Updating its state locally.");
    console.log("Current state: ", this.peers[peers[i]]);

    this.peers[state['address']] = {
      "address": state['address'],
      "status": "ALIVE",
      "timestamp": Date.now()
    };
    console.log("New state: ", this.peers[peers[i]]);
    this.resetInterest();
  }

  for(var i = 0; i < peers.length; i++) {
    if(this.shouldUpdate(state['peers'][peers[i]])) {
      console.log("Updating peer state for host: ", state['peers'][peers[i]]['address']);
      console.log("Current state: ", this.peers[peers[i]]);
      console.log("New state: ", state['peers'][peers[i]]);
      this.peers[peers[i]] = state['peers'][peers[i]];
      this.resetInterest();
    }
  }
}

Chatter.prototype.getOtherPeersByStatus = function(status) {
  return _u.filter(Object.keys(this.peers), function(peer) {return peer != this.address && this.peers[peer]['status'] == status}, this);
}

Chatter.prototype.simpleChatter = function() {
  console.log("Running simpleChatter");
  var peer = this.getRandomPeer();
  if(!peer) return;
  var message = this.prepareMessage();
  var peerState = this.pushpull(peer['address'], message);
  if(peerState) {
    this.updateState(peerState);
  }
}

Chatter.prototype.complexChatter = function() {
  if(!this.isInterested()) {
    console.log("Not interesting in chatting.");
    return;
  }

  console.log("Running complexChatter");

  var peer = this.getRandomPeer();
  if(!peer) return;
  var message = this.prepareMessage();
  var peerState = this.pushpull(peer['address'], message);
  if(peerState) {
    this.updateState(peerState);
  }

}

Chatter.prototype.getRandomPeer = function() {
  var alive = this.getOtherPeersByStatus("ALIVE");
  if(!alive.length) {
    console.log("I am the sole peer as far as I can see.");
    return null;
  } else {
    console.log("Alive peers other than myself: ", alive);
    var randomPeerIndex = Math.floor((Math.random() * alive.length));
    return this.peers[alive[randomPeerIndex]];
  }
}

Chatter.prototype.seedPeers = function(seed) {
  this.address = seed.address;
  var peers = {}
  for(var i = 0; i < seed.peers.length; i++) {
    var hostMetadata = {
      "address": seed.peers[i],
      "status": "ALIVE",
      "timestamp": 0
    };
    peers[seed.peers[i]] = hostMetadata;
  }
  this.peers = peers;

}

Chatter.prototype.pushpull = function(peer, message) {
  console.log("Sending request to : ", peer);
  request({
    url: "http://" + peer + "/PushPull",
    form: message,
    method: "POST"

  }, function(error, response, body) {
    if(this.isPeerDead(error)) {
      console.log("Peer seems to be dead. Setting status. Host: ", peer);
      this.peers[peer] = {
        "address": peer,
        "status": "DEAD",
        "timestamp": Date.now()
      };
      this.resetInterest();
    } else {
      //console.log("Push Pull Response: ", body);
      this.reduceInterest();
    }
  }.bind(this));
}

Chatter.prototype.isPeerDead = function(error) {
  if(error) {
    console.log("There was an error in connection. Details: ", error, "Declaring peer dead.");
  }
  return error; //Hehe. Need to be more specific ofc, right now just assuming any error == peer is dead.
}

Chatter.prototype.isNewPeer = function(peerState) {
  // return true if this is a new peer - or a previously dead peer.
  return (!this.peers[peerState['address']] || this.peers[peerState['address']]['status'] == "DEAD") && peerState['status'] == "ALIVE";
}

Chatter.prototype.shouldUpdate = function(peerState) {
  return this.peers[peerState['address']]['timestamp'] < peerState['timestamp'];
}

Chatter.prototype.resetInterest = function() {
  this._interest = this._defaultInterest;
}

Chatter.prototype.isInterested = function() {
  return this._interest > 0;
}

Chatter.prototype.reduceInterest = function() {
  this._interest -= 1;
}

exports.Chatter = Chatter;
