'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var Chatter = require("./chatter").Chatter;

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

var args = process.argv.slice(2);
var host = args[0];
var port = parseInt(args[1]);

var seed = {
	peers: ['127.0.0.1:5000', '127.0.0.1:5001', '127.0.0.1:5002'],
	address: host + ":" + port
}
var interval = 2000;
var interest = 2;

var c = new Chatter(seed, interval, interest);

app.post('/PushPull', function(req, res) {
	c.updateState(req.body);
	res.send(c.prepareMessage());
})

c.start();

app.listen(port);
